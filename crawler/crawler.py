import logging
import os
from datetime import timedelta

import requests

from models.divar_models import *
from settings import PHOTO_DIR

logger = logging.getLogger(__name__)
handler = logging.FileHandler('./logs/crawler.log')
handler.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
class Crawler:
    CRAWLER_CREATED = 0
    CRAWLER_CRAWLING = 1
    CRAWLER_NO_PROXY = 2
    CRAWLER_FINISHED = 3

    def __init__(self, location, request, category=None, max_num=None):
        self.location = location
        self.request = request
        self.category = category
        self.photo_dir = self.set_image_path()
        self.date_waiter = list()
        self.city = self.get_city()
        self.status = Crawler.CRAWLER_CREATED
        self.max_num = max_num
        self.received_flyers = 0

    def set_image_path(self):
        return PHOTO_DIR + datetime.now().strftime("%y-%m-%d-%H")

    def get_city(self):
        try:
            city = City.get(City.site_id == self.location)
        except Exception as e:
            raise ValueError(e)
        else:
            return city

    @staticmethod
    def calculate_date(date):
        if date == 'لحظاتی پیش' or date == 'دقایقی پیش':
            return datetime.now()
        elif date == 'یک ربع پیش':
            return datetime.now() - timedelta(minutes=15)
        elif date == 'نیم ساعت پیش':
            return datetime.now() - timedelta(minutes=30)
        elif 'ساعت پیش' in date:
            hours = date.replace('ساعت پیش', '').replace(' ', '')
            return datetime.now() - timedelta(hours=int(hours))
        elif 'هفته پیش' in date:
            weeks = date.replace('هفته پیش', '').replace(' ', '')
            return datetime.now() - timedelta(days=int(weeks) * 7)
        else:
            print(date)

    def photo_downloader(self, photo):
        print('downloading photo for %s' % photo.flyer.id)
        while True:
            try:
                print('requesting %s' % photo.photo_url)
                req = requests.get(photo.photo_url)
                file_name = photo.photo_url.split('/')[-1]
                dir = self.photo_dir + '/' + file_name
                print(req.status_code)
                if req.status_code == 200:
                    if not os.path.exists(self.photo_dir):
                        os.makedirs(self.photo_dir)
                    with open(dir, 'wb') as photo_file:
                        photo_file.write(req.content)
                    photo.photo_path = dir
                    photo.is_downloaded = True
                    photo.save()
                else:
                    print('something wrong!')
                return
            except Exception as exc:
                assert not type(Exception).__name__ == 'KeyboardInterrupt'
                if hasattr(exc, 'message'):
                    print(exc.message)
                else:
                    print(exc)
                # if type(exc).__name__ == 'ProxyError':
                #     HttpRequest.set_new_proxy()

    def get_date(self, flyer):
        response = self.request.send(
            'https://post.divar.ir/v1/post/%s' % flyer.token, headers={}, method='GET'
        )
        data = response['widgets']['header']['date']
        return data

    def set_flyers_date(self, date):
        for f in self.date_waiter:
            f.gut_datetime = date
            f.save()

    def get_objects(self, last_date=0):
        body = {
            "jsonrpc": "2.0", "id": 0, "method": "getPostList",
            "params": [[["place2", 0, [str(self.location)]]], last_date]
        }

        headers = {
            'Host': 'search.divar.ir',
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
            'Accept': 'application/json',
            'User-Agent':
                '71|243|iOS10.2|fa-IR|9FBE861914434039B1608F9A13C6944E|iPhone6,1',
            'Accept-Language': 'fa-IR;q=1',
            'Accept-Encoding': 'gzip, deflate'
        }
        ret = self.request.send(
            'https://search.divar.ir/json/', body=body, headers=headers
        )
        return ret.pop('result')

    def get_desc(self, flyer):
        print('getting desc of %s with id %d' % (flyer.token, flyer.id))
        body = {
            "jsonrpc": "2.0", "method": "getPost", "id": "1", "params": [flyer.token]
        }
        headers = {
            'Host': 'post.divar.ir',
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
            'Accept': 'application/json',
            'User-Agent':
                '71|243|iOS10.2|fa-IR|9FBE861914434039B1608F9A13C6944E|iPhone6,1',
            'Accept-Language': 'fa-IR;q=1',
            'Accept-Encoding': 'gzip, deflate'
        }
        ret = self.request.send(
            url='https://post.divar.ir/json/', body=body, headers=headers
        )
        if ret.get('result').get('error', 0) == 404:
            return
        if ret['result']['desc'] != None or ret['result']['desc'] == '':
            flyer.description = ret['result']['desc']

    def get_pictuers(self, flyer):
        print('getting pictuers')
        url = 'https://s100.divarcdn.com/static/pictures/%s.jpg' % flyer.token
        pic = self.request.send(url=url, method='HEAD', headers=None, )
        print(pic)
        counter = 1
        while pic != 404:
            photo = FlyerPhotos.create(photo_url=url, flyer=flyer)
            self.photo_downloader(photo)
            url = 'https://s100.divarcdn.com/static/pictures/' \
                  '%s.%s.jpg' % (flyer.token, counter)
            pic = self.request.send(url=url, method='HEAD', headers='', )
            counter += 1

    def get_contact(self, flyer):
        body = {"jsonrpc": "2.0", "id": 0, "method": "getContact",
                "params": [flyer.token]}
        headers = {
            'Host': 'r.divar.ir',
            'Content-Type': 'application/json',
            'Connection': 'keep-alive',
            'Accept': 'application/json',
            'User-Agent':
                '71|243|iOS10.2|fa-IR|9FBE861914434039B1608F9A13C6944E|iPhone6,1',
            'Accept-Language': 'fa-IR;q=1',
            'Accept-Encoding': 'gzip, deflate'
        }
        result = self.request.send(
            url='https://r.divar.ir/json/', headers=headers, body=body
        )
        return result.pop('result')

    def sequence_finder(self, num, date_list):
        counter = 1
        for i in range(len(date_list)):
            if counter == num:
                return i
            counter *= 2
        return -1

    def sequence_checker(self, spec):
        if ((spec.site_name == 'v10' and spec.title == 'سال') or
                ((spec.site_name == 'v04' or spec.site_name == 'v03')
                 and spec.title == 'تعداد اتاق') or
                (spec.site_name == 'v10' and spec.title == 'سازنده') or
                (spec.site_name == 'v01' and spec.title == 'نوع')
        ):
            return True
        return False

    def skip_checker(self, spec):
        checker_names = [
            ('title', 'عنوان'), ('email', 'ایمیل'), ('phone', 'تلفن'),
            ('desc', 'توضیح'), ('image', 'عکس'), ('place', 'محل'),
            ('v09', 'قیمت'), ('v09', 'قیمت کل'),
        ]
        for name in checker_names:
            if name == (spec.site_name, spec.title):
                return True
        return False

    def check_product(self, spec):
        ret = False
        if spec.site_name == 'v10' and spec.title == 'برند':
            ret = True
        elif spec.site_name == 'v12' and spec.title == 'برند':
            ret = True
        elif spec.site_name == 'v10' and spec.title == 'سازنده':
            ret = True
        return ret

    def get_product(self, category, index):
        products = Product.select().where(Product.category == category.id)
        position = self.sequence_finder(index, products)
        return products[position] if position != -1 else -1

    def get_all_objects(self):
        result = {'error': 0}
        last_date = 0
        print('entering while loop')
        while result.get('error', 0) == 0 and last_date != -1:
            self.status = Crawler.CRAWLER_CRAWLING
            if last_date == 0:
                print('begin getting location %s' % (self.city.name))
            else:
                print('getting location %s with date %s' % (self.city.name, last_date))
            result = self.get_objects(last_date)
            last_date = result.get('last_post_date', -1)
            date = None

            for post in result.get('post_list'):
                category_hierarchy = list()
                print('getting flyer with token %s' % post.get('token'))
                fields = [CategorySpec.select().where(CategorySpec.category == 1)]
                category = None

                if not post.get('c1') == None:
                    category = Category.get(Category.site_id == post.get('c1'))
                    category_hierarchy.append(category.id)
                    fields.append(
                        CategorySpec.select().where(CategorySpec.category == category)
                    )
                if not post.get('c2') == None:
                    category = Category.get(Category.site_id == post.get('c2'))
                    category_hierarchy.append(category.id)
                    fields.append(
                        CategorySpec.select().where(CategorySpec.category == category)
                    )
                if not post.get('c3') == None:
                    category = Category.get(Category.site_id == post.get('c3'))
                    category_hierarchy.append(category.id)
                    fields.append(
                        CategorySpec.select().where(CategorySpec.category == category)
                    )
                if not post.get('c4') == None:
                    category = Category.get(Category.site_id == post.get('c4'))
                    category_hierarchy.append(category.id)
                    fields.append(
                        CategorySpec.select().where(CategorySpec.category == category)
                    )
                product = Product.get(Product.title == 'سایر محصولات')
                all_specs = list()
                checker = set()
                for field_peace in fields:
                    for field in field_peace:
                        spec_field = Specification.get(
                            Specification.id == field.spec.id
                        )
                        if spec_field.id not in checker and spec_field.site_name != None:
                            all_specs.append(spec_field)
                            checker.add(spec_field.id)
                try:
                    defaults = {
                        'title': post.get('title'),
                        'description': post.get('desc', ''),
                        'phone_number': 0,
                        'price': post.get('v09', -3),
                        'city': self.city,
                        'category': category,
                        'status': Flyer.CREATED,
                        'product': product
                    }
                    flyer, is_created = Flyer.get_or_create(
                        token=post.get('token'), defaults=defaults
                    )
                    self.received_flyers += 1
                except Exception as exc:
                    if hasattr(exc, 'message'):
                        msg = exc.message
                    else:
                        msg = str(exc)
                    logger.error(
                        'save flyer instance: token is %s ,%s with exception %s'  %
                        (post.get('token'), defaults, msg),
                    )
                    continue

                if self.max_num is not None and self.max_num >= self.received_flyers:
                    self.status = Crawler.CRAWLER_FINISHED
                    print('finished')
                    return
                if is_created:

                    self.get_desc(flyer)
                    contact = self.get_contact(flyer)
                    if contact is None or contact.get('error') == 800:
                        print('no proxy')
                        self.status = Crawler.CRAWLER_NO_PROXY
                        return
                    if flyer.price is not None and flyer.price >= -1 and contact.get('phone', '') != '':
                        flyer.status = Flyer.READY

                    flyer.phone_number = contact.get('phone', 0)
                    flyer.email = contact.get('email')
                    try:
                        flyer.save()
                    except Exception as exc:
                        logger.error(
                            'exception for saving flyer exc %s with values %s' % (str(exc), [
                                flyer.price, flyer.description, flyer.phone_number, flyer.email, flyer.token,
                                contact, post
                            ])
                        )
                else:
                    self.status = Crawler.CRAWLER_FINISHED
                    return

                date_temp = Crawler.calculate_date(self.get_date(flyer))
                if date is None:
                    date = date_temp
                if date == date_temp:
                    self.date_waiter.append(flyer)
                else:
                    self.set_flyers_date(date)
                    date = Crawler.calculate_date(self.get_date(flyer))

                self.get_pictuers(flyer)
                for field in all_specs:
                    print('getting field %s' % field.title)
                    if post.get(field.site_name) == None or self.skip_checker(field):
                        print('field has nothing')
                        continue
                    if field.is_choice:
                        print('field is choice able')
                        all_spec_values = SpecValues.select().where(
                            SpecValues.spec == field.id,
                        )
                        spec_values = []
                        for spec in all_spec_values:
                            if spec.category.id in category_hierarchy:
                                spec_values.append(spec)

                        if type(post.get(field.site_name)) == int:
                            index = self.sequence_finder(
                                int(post.get(field.site_name)), spec_values
                            )
                            if index == -1:
                                print(
                                    'wrong index %s for flyer %s' % (post.get(field.site_name), flyer.id)
                                )
                                continue
                            spec_value = spec_values[index]
                        elif type(post.get(field.site_name)) == str:
                            for spec in spec_values:
                                if spec.value == post.get(field.site_name):
                                    spec_value = spec
                        flyer_spec = FlyerSpec.create(
                            flyer=flyer, spec=field, spec_value=spec_value
                        )

                    else:
                        print('field is not choice able')
                        print('value is %s' % post.get(field.site_name))
                        if post.get(field.site_name):
                            flyer_spec = FlyerSpec.create(
                                text=post.get(field.site_name),
                                flyer=flyer, spec=field
                            )
        print('exiting while loop')
        self.status = Crawler.CRAWLER_FINISHED

    def start_crawl(self):
        self.get_all_objects()

    def get_status(self):
        return self.status
