from .mapper import Mapper


def start():
    mapper = Mapper()
    mapper.start_mapping()
