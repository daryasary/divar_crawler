from models.Rahnama_models import *
from models.divar_models import *


class Mapper:
    def __init__(self):
        self.missed = 0
        self.inserted = 0

    @staticmethod
    def find_category_rahnama_code(category):
        cat = category
        while cat.rahnama_code is None and cat.parent is not None:
            cat = cat.parent
        print(cat.title, cat.id)
        return cat.rahnama_code

    @staticmethod
    def check_rent(flyer):
        if flyer.category.parent.id in [34, 48]:
            return True
        return False

    @staticmethod
    def check_range(spec):
        if spec.title in ['متراژ (متر مربع)', 'ودیعه', 'اجاره ماهیانه', 'کارکرد (کیلومتر)']:
            return True
        return False

    def spec_mapper(self, flyer):
        specs = FlyerSpec.select().where(FlyerSpec.flyer == flyer.id)
        for spec in specs:
            specification = Specification.get(Specification.id == spec.spec.id)
            if specification.rahnama_code is None:
                print('spec %d %s has no code' % (specification.id, specification.title))
                continue
            if spec.text is None:
                print('spec %d %s has no text' % (specification.id, specification.title))
                spec_model = SpecValues.get(SpecValues.id == spec.spec_value)
                print('value is %s' % spec_model.value)
                RahnamaFlyerSpec.create(
                    flyer_id=flyer.rahnama_code, spec_id=specification.rahnama_code,
                    spec_value_id=spec_model.rahnama_code, spec_text=''
                )
            elif spec.text is not None and self.check_range(specification):
                print('spec is range and has no value %d %s' % (
                    specification.id, specification.title))
                try:
                    range = SpecRange.get(
                        SpecRange.spec == specification.id,
                        SpecRange.start_value <= int(spec.text) and
                        SpecRange.end_value >= int(spec.text)
                    )
                except SpecRange.DoesNotExist:
                    if int(spec.text) >= 999999999:
                        range = SpecRange.get(
                            SpecRange.spec == specification.id,
                            SpecRange.start_value <= int(spec.text) and
                            SpecRange.end_value >= 999999999
                        )
                print('range is %d' % range.id)
                RahnamaFlyerSpec.create(
                    flyer_id=flyer.rahnama_code, spec_id=specification.rahnama_code,
                    spec_value_id=range.rahnama_code, spec_text=''
                )

    @staticmethod
    def photo_mapper(flyer):
        print('getting photo %s %s' % (flyer.id, flyer.token))
        photos = FlyerPhotos.select().where(
            FlyerPhotos.flyer == flyer.id,
            FlyerPhotos.photo_path is not None, FlyerPhotos.is_downloaded == True
        )
        for photo in photos:
            model = RahnamaFlyerPhoto(photo=photo.photo_path, flyer_id=flyer.rahnama_code)
            model.save()

    @staticmethod
    def category_hierarchy(flyer):
        category = flyer.category
        hierarchy = list()
        while category is not None:
            hierarchy.append(category.id)
            category = category.parent
        return hierarchy

    def calculate_rent_price(self, flyer, rahnama_flyer):
        hierarchy = self.category_hierarchy(flyer)
        if Category.get(title='اجاره مسکونی (آپارتمان، خانه، زمین)').id in hierarchy or \
                Category.get(
                    title=
                    'اجاره اداری و تجاری (مغازه، دفتر کار، صنعتی)').id in hierarchy:
            print(flyer.category.id)
            print(flyer.category.title)
            print(flyer.category.parent.id)
            print(flyer.category.parent.title)
            try:
                spec = Specification.get(title='اجاره ماهیانه', category=flyer.category)
            except Specification.DoesNotExist:
                spec = Specification.get(title='اجاره ماهیانه',
                                         category=flyer.category.parent)
            try:
                rahnama_flyer.price += int(int(FlyerSpec.get(
                    FlyerSpec.flyer == flyer.id, FlyerSpec.spec == spec
                ).text) / 30000) * 1000000
            except FlyerSpec.DoesNotExist:
                return

    def mapper(self, flyer):
        print('mapping flyer %s %s' % (flyer.id, flyer.token))
        category = Category.get(Category.id == flyer.category)
        location = City.get(City.id == flyer.city)
        category_code = self.find_category_rahnama_code(category)
        if category_code is None:
            print('category %s %d doesnt have any code' % (
                flyer.category.title, flyer.category.id))
            return
        email = ''
        price = flyer.price
        if price == -1:
            price = 0
        if flyer.email:
            email = email
        print(category_code)
        model = RahnamaFlyer(
            title=flyer.title, description=flyer.description, price=price,
            category_id=category_code,
            location_id=location.rahnama_code,
            seller_mobile='98%d' % flyer.phone_number,
            seller_email=email, status=10,
            updated_time=datetime.now(), exchangeable='', latitude=None, longitude=None
        )
        self.calculate_rent_price(flyer, model)
        model.save()
        flyer.rahnama_code = model.id
        flyer.status = Flyer.INSERTED
        flyer.save()
        self.photo_mapper(flyer)
        self.spec_mapper(flyer)

    def check_skip(self, flyer):
        l = []
        if flyer.category.id in l:
            print('flyer %d %s skipped' % (flyer.id, flyer.token))
            self.missed += 1
            return True
        return False

    def check_missed(self, flyer):
        if flyer.rahnama_code is None:
            self.missed += 1
            print('flyer with token %s missed' % flyer.token)
        else:
            self.inserted += 1

    def start_mapping(self):
        for flyer in Flyer.select().where(Flyer.status == Flyer.READY):
            if self.check_skip(flyer):
                continue
            self.mapper(flyer)
            self.check_missed(flyer)
        print('added %s' % self.inserted)
        print('%s missed' % self.missed)
