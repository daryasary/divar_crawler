import json

from models.divar_models import *


class Parser:
    def __init__(self):
        with open('Divar_deatails.json') as data_file:
            self.data = json.load(data_file)

    def parse(self):
        for object in self.data:
            if object == 'places':
                print('creating places')
                self.places_parser(self.data.get(object))
            elif object == 'categories':
                print('creating categories')
                self.categories_parser(self.data.get(object))

    def places_parser(self, data):
        levels = dict()
        for object in data:
            level = int(object.get('level')[-1])
            if level == 2:
                City.create(name=object.get('name'), site_id=object.get('id'))
                print('city %s created' % object.get('name'))

    def categories_parser(self, data):
        levels = dict()
        for object in data:
            level = int(object.get('level')[-1])
            levels[level] = levels.get(level, list()) + [object]

        for i in range(max(levels)+1):
            categories = levels[i]
            for category in categories:
                parent = None
                if i > 0:
                    parent = Category.get(Category.site_id == category.get('parent_id'))
                c = Category.create(
                    title=category.get('name'), site_id=category.get('id'), level=i,
                    parent=parent
                )
                for field in category.get('fields'):
                    choice = False
                    if field.get('type') == 'choice':
                        choice = True
                    s, is_s_created = Specification.get_or_create(
                        title=field.get('title'), site_name=field.get('corresponding'),
                        is_choice=choice, category=c
                    )
                    cs = CategorySpec.create(
                        is_required=not field.get('flags').get('is_optional'), category=c,
                        spec=s
                    )
                    if field.get('type') == 'choice':
                        for choice in field.get('choices'):
                            print(choice)
                            ch, is_created = SpecValues.get_or_create(
                                spec=s, value=choice, category=c
                            )

        Product.create(
            title='سایر محصولات',
            category=Category.get(title='همهٔ آگهی‌ها')
        )
        c1 = Category.get(title='فروش مسکونی (آپارتمان، خانه، زمین)')
        c2 = Category.get(title='اجاره مسکونی (آپارتمان، خانه، زمین)')
        s1 = Specification.create(title='نوع ساختمان', is_choice=True, category=c1)
        s2 = Specification.create(title='نوع ساختمان', is_choice=True, category=c2)
        SpecValues.create(
            spec=s1, value='آپارتمان', category=c1
        )
        SpecValues.create(
            spec=s1, value='خانه و ویلا', category=c1
        )
        SpecValues.create(
            spec=s1, value='زمین و کلنگی', category=c1
        )
        SpecValues.create(
            spec=s2, value='آپارتمان', category=c2
        )
        SpecValues.create(
            spec=s2, value='خانه و ویلا', category=c2
        )

