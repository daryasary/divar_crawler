from crawler.crawler import Crawler
from models.divar_models import *


class Checker(Crawler):
    def get_all_objects(self):
        print('check flyers for missing items, location %s' % self.location)
        for flyer in Flyer.select().where(Flyer.phone_number == None, Flyer.city == self.city.id):
            print('flyer with token %s has no phone number' % flyer.token)
            contact = self.get_contact(flyer)
            if contact is None or contact.get('error') == 800:
                print('no proxy')
                self.status = Crawler.CRAWLER_NO_PROXY
                return
            flyer.phone_number = contact.get('phone', '')
            flyer.save()

        for flyer in Flyer.select().where(
                Flyer.description == None, Flyer.description == ''):
            print('flyer with token %s has no desc' % flyer.token)
            self.get_desc(flyer)
            flyer.save()