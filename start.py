import getopt
import sys

from crawler.handler import start as crawler_init
from initializer.handler import start as start_init
from mapper.handler import start as start_mapper
from models.migrations import create_table
from removal.handler import start as start_removal


def main(argv):
    """Main process handler of project, will have different treats on
    modes:
        Usage: python start.py --mod <mod_num>
        mod options:
            0 : Create tables, (may run once just at beginning of project)
            1 : Parse constants json
            2 : Initiate crawler and save flyers to the database
            3 : Insert scrapped flyers to the rahnama db
            4 : Suspend flyers that are removed from divar
            5 : 2,3,4 steps together"""
    try:
        opts, args = getopt.getopt(argv, "", ["mod="])
    except getopt.GetoptError:
        print('main.py --mod <mod_num>')
        sys.exit(2)

    step_mod = None
    for opt, arg in opts:
        if opt == '--mod':
            step_mod = arg

    if step_mod == '0':
        create_table()

    if step_mod == '1':
        start_init()

    if step_mod == '2':
        crawler_init()

    if step_mod == '3':
        start_mapper()

    if step_mod == '4':
        start_removal()

    if step_mod == '5':
        crawler_init()
        start_mapper()
        start_removal()

    print('###########################DONE##################################')


if __name__ == '__main__':
    main(sys.argv[1:])
