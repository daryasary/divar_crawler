import datetime

import peewee as pw

from local_settings import *
from settings import DEFAULT_SELLER_NAME, DIVAR_USER_ID, SAMPLE_UUID

rahnama_db = pw.MySQLDatabase(RAHNAMA_NAME, **RAHNAMA)


class RahnamaFlyer(pw.Model):
    created_time = pw.DateTimeField(default=datetime.datetime.now())
    title = pw.CharField()
    description = pw.TextField()
    seller_name = pw.CharField(default=DEFAULT_SELLER_NAME)
    seller_mobile = pw.BigIntegerField(null=True)
    seller_tel = pw.CharField(null=True, default='')
    seller_email = pw.CharField(null=True)
    status = pw.SmallIntegerField(null=True)
    price = pw.BigIntegerField(null=True)
    category_id = pw.IntegerField(null=True)
    updated_time = pw.DateTimeField(null=True)
    exchangeable = pw.CharField(null=True)
    # sms_purchase_code = pw.CharField(null=True)
    # seller_telegram = pw.CharField(null=True)
    sell_page_url = pw.CharField(default="")
    verify_codes = pw.CharField(default="")
    user_id = pw.IntegerField(default=DIVAR_USER_ID)
    sms_purchase_code = pw.CharField(default="")
    sms_purchase_numb = pw.CharField(default="")
    seller_telegram = pw.CharField(default="")
    price_original = pw.IntegerField(default=None)
    one_signal_id = pw.CharField(default=SAMPLE_UUID)
    location_id = pw.IntegerField(null=True)
    latitude = pw.DecimalField(null=True)
    longitude = pw.DecimalField(null=True)

    class Meta:
        database = rahnama_db
        db_table = 'flyers'


class RahnamaFlyerSpec(pw.Model):
    spec_text = pw.CharField()
    flyer_id = pw.IntegerField()
    spec_id = pw.IntegerField()
    spec_value_id = pw.IntegerField()

    class Meta:
        database = rahnama_db
        db_table = 'flyers_specs'


class RahnamaFlyerPhoto(pw.Model):
    created_time = pw.DateTimeField(default=datetime.datetime.now())
    photo = pw.CharField()
    flyer_id = pw.IntegerField()

    class Meta:
        database = rahnama_db
        db_table = 'flyers_photos'
