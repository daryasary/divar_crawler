from models.divar_models import *


def create_table():
    models_list = [
        State, City, Category, Product, Specification, CategorySpec, ProductSpecValue,
        SpecValues, Flyer, FlyerSpec, FlyerPhotos, SpecRange
    ]

    for model in models_list:
        if model.table_exists():
            model.drop_table(fail_silently=True)
        model.create_table()


if __name__ == '__main__':
    create_table()
