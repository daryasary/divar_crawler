from datetime import datetime

from peewee import *

from local_settings import DIVAR_NAME, DIVAR

db = MySQLDatabase(DIVAR_NAME, **DIVAR)


class BaseModel(Model):
    rahnama_code = IntegerField(null=True)
    created_time = DateTimeField(default=datetime.now)
    uploaded_time = DateTimeField(null=True)

    class Meta:
        database = db


class State(BaseModel):
    name = CharField(max_length=64)


class City(BaseModel):
    name = CharField(max_length=64)
    site_id = IntegerField()
    state = ForeignKeyField(State, related_name='cities', null=True)


class Category(BaseModel):
    parent = ForeignKeyField('self', related_name='childes', null=True)
    title = CharField(max_length=256)
    site_id = IntegerField()
    level = IntegerField()

    def __str__(self):
        return self.title


class Product(BaseModel):
    title = CharField(max_length=256)
    category = ForeignKeyField(Category, related_name='products')

    def __str__(self):
        return self.title


class Specification(BaseModel):
    title = CharField(max_length=256)
    site_name = CharField(max_length=512, null=True)
    is_choice = BooleanField(default=False)
    category = ForeignKeyField(Category, related_name='specs')


class CategorySpec(Model):
    is_required = BooleanField(default=False)
    category = ForeignKeyField(Category)
    spec = ForeignKeyField(Specification)

    class Meta:
        database = db


class ProductSpecValue(Model):
    product = ForeignKeyField(Product)
    spec = ForeignKeyField(Specification)

    class Meta:
        database = db


class SpecValues(BaseModel):
    value = TextField()
    category = ForeignKeyField(Category)
    spec = ForeignKeyField(Specification, related_name='values')


class Flyer(BaseModel):
    CREATED = 0
    INSERTED = 1
    REMOVED = 2
    HIDE = 3
    READY = 4

    title = CharField(max_length=512)
    description = CharField(max_length=1024)
    phone_number = BigIntegerField()
    price = IntegerField(null=True)
    city = ForeignKeyField(City, related_name='flyers')
    product = ForeignKeyField(Product, related_name='products')
    category = ForeignKeyField(Category, related_name='categories')
    status = IntegerField()
    token = CharField(max_length=64)
    email = CharField(max_length=512, null=True)
    gut_datetime = DateTimeField(default=datetime.now)


class FlyerSpec(BaseModel):
    text = CharField(max_length=256, null=True)
    flyer = ForeignKeyField(Flyer)
    spec = ForeignKeyField(Specification)
    spec_value = ForeignKeyField(SpecValues, null=True)


class FlyerPhotos(BaseModel):
    photo_url = CharField(max_length=512)
    photo_path = CharField(max_length=512, null=True)
    is_downloaded = BooleanField(default=False)
    flyer = ForeignKeyField(Flyer, related_name='photos')


class SpecRange(BaseModel):
    start_value = IntegerField()
    end_value = IntegerField()
    spec = ForeignKeyField(Specification, related_name='ranges')
