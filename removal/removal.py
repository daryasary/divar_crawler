from models.divar_models import Flyer
from models.Rahnama_models import RahnamaFlyer
from utils import HttpRequest


class Removal:
    def __init__(self, location):
        self.removed = 0
        self.location = location
        self.request = HttpRequest()

    def get_removed_objects(self):
        print('getting removed objects')
        for flyer in Flyer.select().where(Flyer.status == Flyer.INSERTED, Flyer.city == self.location):
            body = {
                "jsonrpc": "2.0", "method": "getPost", "id": "1", "params": [flyer.token]
            }
            headers = {
                'Host': 'post.divar.ir',
                'Content-Type': 'application/json',
                'Connection': 'keep-alive',
                'Accept': 'application/json',
                'User-Agent':
                    '71|243|iOS10.2|fa-IR|9FBE861914434039B1608F9A13C6944E|iPhone6,1',
                'Accept-Language': 'fa-IR;q=1',
                'Accept-Encoding': 'gzip, deflate'
            }
            ret = self.request.send(
                url='https://post.divar.ir/json/', body=body, headers=headers
            )
            if ret.get('error', 0) == 301 or ret.get('error', 0) == 404:
                print('flyer %s removed' % flyer.token)
                flyer.status = Flyer.REMOVED
                flyer.save()
            else:
                print('flyer %s still exists in divar' % flyer.token)

    def hide_removed_flyers(self):
        print('hiding removed flyers')
        for flyer in Flyer.select().where(Flyer.status == Flyer.REMOVED, Flyer.city == self.location):
            rahnama_flyer = RahnamaFlyer.get(id=flyer.rahnama_code)
            print('flyer %s removed' % rahnama_flyer.id)
            flyer.status = Flyer.HIDE
            rahnama_flyer.status = 20
            rahnama_flyer.save()
            self.removed += 1
        print('%s flyers removed' % self.removed)

    def start(self):
        self.get_removed_objects()
        self.hide_removed_flyers()

