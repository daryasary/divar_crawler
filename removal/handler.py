from queue import Queue
from threading import Thread, Event

from models.divar_models import City
from settings import MAX_THREADS
from .removal import Removal


class Handler:
    def __init__(self):
        self.queue = Queue()
        self.threads = []
        self.event = Event()
        self.closing = False

    def close_all(self):
        with self.queue.mutex:
            if self.closing:
                return
            self.event.clear()
            self.queue.queue.clear()
            self.queue.unfinished_tasks = 0
            self.queue.all_tasks_done.notify_all()
            self.closing = True

    def worker(self):
        while not self.queue.empty():
            location = self.queue.get()
            print('starting removing for location %s' % location)
            c = Removal(location)
            c.start()
            self.queue.task_done()

    def closing_threads(self):
        for thread in self.threads:
            print('joining threads')
            thread.join()

    def start(self):
        try:
            self.event.set()
            for city in City.select():
                self.queue.put(city.site_id)

            for i in range(MAX_THREADS):
                thread = Thread(target=self.worker)
                thread.daemon = True
                self.threads.append(thread)
                thread.start()
            self.queue.join()
            self.event.clear()
            self.closing_threads()
            print('done!')
        except KeyboardInterrupt:
            self.event.clear()
            self.close_all()
            self.closing_threads()


def start():
    removal = Handler()
    removal.start()
