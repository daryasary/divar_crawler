import logging
from threading import Lock

import requests
from requests.exceptions import ProxyError

from settings import PROXIES

lock = Lock()

logger = logging.getLogger(__name__)
handler = logging.FileHandler('./logs/utils.log')
handler.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

class HttpRequest:

    def __init__(self):
        self.proxy = self.set_proxy()

    def set_proxy(self):
        if len(PROXIES):
            proxy = PROXIES.pop()
            print("Proxy {} assigned".format(proxy))
            return proxy
        print("Set proxy failed, crawl will follow with real IP address")
        return None

    def set_new_proxy(self):
        lock.acquire()
        flag = False
        if len(PROXIES):
            print('setting new proxy')
            self.proxy = PROXIES.pop()
            print("Proxy {} assigned".format(self.proxy))
            flag = True
        lock.release()
        return flag

    def send(self, url, headers, method='POST', body=None):
        while True:
            try:
                if method == 'HEAD':
                    req = requests.head(url, proxies=self.proxy, )
                    ret = req.status_code
                    return ret
                else:
                    req = requests.request(
                        method=method, url=url, headers=headers, json=body,
                        proxies=self.proxy,
                    )
                    ret = req.json()
                if ret.get('result', dict()).get('error', 0) == 800:
                    if self.set_new_proxy():
                        continue
                return ret
            except ProxyError:
                self.set_new_proxy()
            except Exception as exc:
                assert not type(exc).__name__ == 'KeyboardInterrupt'
                logging.error(str(exc))
